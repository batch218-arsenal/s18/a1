// console.log("Hello World");

function addNums(num1,num2){
	console.log(num1 + num2);
};

function subNums(num1,num2){
	console.log(num1 - num2);
};


console.log("Displayed sum of 5 and 15");
addNums(5,15);

console.log("Displayed difference of 20 and 5");
subNums(20,5);


function multiplyNums(num1,num2){
	return num1*num2;
};

function divideNums(num1,num2){
	return num1/num2;
};


let product = multiplyNums(50,10);
let quotient = divideNums(50,10);

console.log("The product of 50 and 10:");
console.log(product);
console.log("The quotient of 50 and 10:");
console.log(quotient);


function getAreaOfCircle(radius){
//	using area = 3.1416*(radius**2);

	return 3.1416*(radius**2);
};


let circleArea = getAreaOfCircle(15);

console.log("The result of getting the area of a circle with 15 radius:");
console.log(circleArea);


function getAverage(num1,num2,num3,num4){

	return (num1+num2+num3+num4)/4;
};


let averageVar = getAverage(20,40,60,80);
console.log("The average of 20,40,60 and 80: ");
console.log(averageVar);


function checkIfPassed(score,totalScore){

	let isPassed = (((score/totalScore)*100) >= 75);
	return isPassed;

};


let isPassingScore = checkIfPassed(38,50);
console.log("Is 38/50 a passing score?")
console.log(isPassingScore);

